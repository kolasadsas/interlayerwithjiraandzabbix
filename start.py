import requests
import json
from jira.client import JIRA
from flask import Flask
from pyzabbix import ZabbixAPI, ZabbixAPIException

from consts import SERVER_JIRA, PASSWORD_JIRA, LOGIN_JIRA, SERVER_ARP, USER_ARP, PASSWORD_ARP, SERVER_ZABBIX, \
    LOGIN_ZABBIX, PASSWORD_ZABBIX

app = Flask(__name__)


@app.route('/arp/')
def getIpFromLogin():
    jira_options = {'server': SERVER_JIRA}
    jira = JIRA(options=jira_options, basic_auth=(LOGIN_JIRA, PASSWORD_JIRA))
    issue = jira.issue('')
    user = issue.fields.customfield_10001

    response = requests.post(SERVER_ARP, data={'username': USER_ARP, 'password': PASSWORD_ARP})
    token = json.loads(response.text)['token']
    response = requests.get('http:///api/abon_ip/%s/?Token=%s' % (user, token))
    ip = json.loads(response.text)['ip']

    z = ZabbixAPI(SERVER_ZABBIX)
    z.login(user=LOGIN_ZABBIX, password=PASSWORD_ZABBIX)

    try:
        z.do_request(method="host.create", params={

            "host": "blablalbalba",

            "interfaces": [

                {

                    "type": 1,

                    "main": 1,

                    "useip": 1,

                    "ip": ip,

                    "dns": "",

                    "port": "10050"

                }

            ],

            "groups": [

                {

                    "groupid": "37"

                }

            ],

            "templates": [

                {

                    "templateid": "10104"

                }

            ],

        }

                     )
    except ZabbixAPIException:
        ze = ZabbixAPIException('dfgfdgfg')
        print(ze)

    return None


if __name__ == '__main__':
    app.run(debug=True)
