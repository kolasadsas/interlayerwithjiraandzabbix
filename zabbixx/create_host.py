from pyzabbix import ZabbixAPI

from consts import SERVER_ZABBIX, LOGIN_ZABBIX, PASSWORD_ZABBIX
from flaskk.test import getIpFromLogin

z = ZabbixAPI(SERVER_ZABBIX)

z.login(user=LOGIN_ZABBIX, password=PASSWORD_ZABBIX)

ip = getIpFromLogin()

z.do_request(method="host.create", params={

    "host": "blablalbalba",

    "interfaces": [

        {

            "type": 1,

            "main": 1,

            "useip": 1,

            "ip": ip,

            "dns": "",

            "port": "10050"

        }

    ],

    "groups": [

        {

            "groupid": ""

        }

    ],

    "templates": [

        {

            "templateid": "10104"

        }

    ],

}

             )